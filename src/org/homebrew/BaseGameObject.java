package org.homebrew;

public class BaseGameObject {
    // entity position
    protected Point2D pos;
    // character representing the entity
    private char character;

    // class constructor
    BaseGameObject(char character) {
        setCharacter(character);
        pos = new Point2D(0,0);
    }

    // class constructor
    BaseGameObject(char character, int x, int y) {
        setCharacter(character);
        pos = new Point2D(x,y);
    }

    // character property
    public char character() {return character;}
    public void setCharacter(char character) {this.character = character;}

    public void draw(java.awt.Graphics g) {
        g.drawString(String.valueOf(this.character()), pos.X(), pos.Y());
    }
}
