package org.homebrew;

public class Point2D {
    private int x;
    private int y;

    // class constructor
    Point2D(int x, int y) {
        setX(x);
        setY(y);
    }

    // x property
    public int X() {return x;}
    public void setX(int x) {this.x = x;}

    // y property
    public int Y() {return y;}
    public void setY(int y) {this.y = y;}
}
