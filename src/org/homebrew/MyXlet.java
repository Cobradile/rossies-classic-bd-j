package org.homebrew;

import java.awt.*;
import java.net.*;

import javax.media.*;
import javax.tv.xlet.*;
import org.bluray.net.BDLocator;
import org.davic.media.MediaLocator;
import org.havi.ui.HScene;
import org.havi.ui.HSceneFactory;
import org.bluray.ui.event.HRcEvent;
import org.dvb.event.UserEvent;
import org.dvb.event.EventManager;
import org.dvb.event.UserEventListener;
import org.dvb.event.UserEventRepository;


public class MyXlet extends Container implements Xlet, Runnable,UserEventListener {

    private HScene scene;
    private Font font;

    Player videoplayer = null;

    // player object
    BaseGameObject player;
    private Image playerSprite;
    private int x = 100, direction = 0;
    private boolean running = true;

    public void initXlet(XletContext context) {
        try
        {
            MediaLocator locator = new MediaLocator(new BDLocator(null, -1, 0));
            this.videoplayer = Manager.createPlayer(locator);
            this.videoplayer.start();
        }
        catch (Exception localException) {}

        setSize(1280, 720);
        scene = HSceneFactory.getInstance().getDefaultHScene();
        scene.add(this);

        URL shotgunSprite = getClass().getResource("/sprites/01a.png");
        playerSprite = getToolkit().getImage(shotgunSprite);

        // setup the player
        player = new BaseGameObject('@', 50, 50);

        UserEventRepository userEventRepo = new UserEventRepository("x");
        userEventRepo.addAllArrowKeys();
        EventManager.getInstance().addUserEventListener(this, userEventRepo);
        scene.validate();
    }

    public void startXlet() {
        setVisible(true);
        scene.setVisible(true);
        new Thread(this).start();
    }

    public void pauseXlet() {
        setVisible(false);
    }

    public void destroyXlet(boolean unconditional) {
        scene.remove(this);
        scene = null;
    }

    public void run()
    {
        while (running) {
            x += direction * 10;
            repaint();
            scene.repaint();
            try {Thread.sleep(15);} catch (Exception e) {}
        }
    }

    public void userEventReceived(UserEvent evt)
    {
        if (evt.getType() == HRcEvent.KEY_PRESSED) {
            switch (evt.getCode()) {
                case HRcEvent.VK_LEFT:
                    //	messages.add("HRcEvent.VK_LEFT : " + evt.getCode());
                    direction = -1;
                    break;

                case HRcEvent.VK_RIGHT:
                    //	messages.add("HRcEvent.VK_RIGHT : " + evt.getCode());
                    direction = 1;
                    break;
            }
        }
        if (evt.getType() == HRcEvent.KEY_RELEASED)
        {
            switch (evt.getCode())
            {
                case HRcEvent.VK_LEFT:
                case HRcEvent.VK_RIGHT:
                direction = 0;
                break;
            }
        }
    }

    public void paint(Graphics g) {
        g.setColor(new Color(0x000000));
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setColor(new Color(0xffffff));
        if (font == null) {
            font = new Font(null, Font.PLAIN, 30);
            g.setFont(font);
        }
        String s=Integer.toString(x);
        g.drawString(s, 50, 50);
        g.drawImage(playerSprite, x, 940, null);
        player.draw(g);
    }
}
